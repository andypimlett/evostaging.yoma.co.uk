$(document).ready(function () {

    // media query event handler
	if (matchMedia) {
	  var mq = window.matchMedia("(min-width: 768px)");
	  mq.addListener(WidthChange);
	  WidthChange(mq);
	}

	// media query change
	function WidthChange(mq) {

	  if (mq.matches) {
	    //initialize swiper when document ready  
		var mySwiper = new Swiper ('.swiper-container', {
		 	// Optional parameters
		 	autoplay: 5500,
		 	speed: 1000,
		    spaceBetween: 500,
		 	nextButton: '.custom-button-next',
			prevButton: '.custom-button-prev',
			});

	    var swiper = new Swiper('.swiper-products', {
		    autoplay: 3500,
		    pagination: '.swiper-pagination',
		    slidesPerView: 5,
		    centeredSlides: true,
		    paginationClickable: true,
		    spaceBetween: 30,
		    loop: true
	   });
	  } else {
	  	//initialize swiper when document ready  
		var mySwiper = new Swiper ('.swiper-container', {
		 	// Optional parameters
		 	autoplay: 5500,
		 	speed: 1000,
		    spaceBetween: 500
			});

	    var swiper = new Swiper('.swiper-products', {
		    autoplay: 3500,
		    pagination: '.swiper-pagination',
		    slidesPerView: 1,
		    centeredSlides: true,
		    paginationClickable: true,
		    spaceBetween: 30,
		    loop: true
	    });
	  }

	}

	$( "#mobile-nav" ).click(function() {
  		$( ".main-nav" ).animate({
    	left: "+=50",
    	height: "toggle"
  	}, 500, function() {
    // Animation complete.
  		});
	});

	//Smooth Scrolling
	jQuery(function($){
            $('a[href*="#"]:not([href="#"])').click(function() {
                if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                    if (target.length) {
                        $('html,body').animate({
                        scrollTop: target.offset().top
                        }, 1000);
                        return false;
                    }
                }
            });
        });        
});